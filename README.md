DM164140 development board (General Purpose Xpress Board) based on PIC16F18855 Microcontroller.

Datasheet: https://ww1.microchip.com/downloads/en/DeviceDoc/Microchip%208bit%20mcu%20PIC%20Xpress%20Dev%20Board%20PIC16F18855%20MPLAB%20Xpress%20Eval%20Board%20Users%20Guide%2050002479B.pdf

![title](picboard.png)

1. PIC® MCU Input/Output Connections
2. PIC16F18855 Microcontroller
3. mikroBUS™ Click Board Socket
4. LEDs
5. Potentiometer
6. Push Button
7. Master Clear Reset Button
8. Battery Connection
9. Micro USB Connector
10. Status LED (Green = Running, Red = Programming)
